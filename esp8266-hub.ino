#include <ESP8266WiFi.h>
#include <WiFiUDP.h>

/* Host network */
const char ssid[] = "HostNetworkSSID";
const char password[] = "HostNetworkPassword";
unsigned int port = 3000;
WiFiUDP udp;
char incomingPacket[100];

/* softAP network */
const char APssid[] = "SoftNetworkSSID";
const char APpassword[] = "SoftNetworkPassword";

/* statsd info */
IPAddress statsdIP = IPAddress(192,168,1,128);
unsigned int statsdPort = 8125;

/* quick counter */
unsigned int loopCounter = 0;
unsigned int DELAY = 100;

void setup() {
  Serial.begin(119200);

  // initialize the host/local network
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);
  while (WiFi.waitForConnectResult() != WL_CONNECTED) {
    ESP.restart();
  }

  // initialize the ESP network
  if (!WiFi.softAP(APssid, APpassword)) {
    ESP.restart();
  }

  // UDP available on both networks!
  udp.begin(port);
}

const char delimiter[] = ":";
const char strcmpr[] = "hub:";

void loop() {
  // parses incoming UDP packets, and then retransmits them to the statsd server
  int packetSize = udp.parsePacket();
  if (packetSize) {
    int len = udp.read(incomingPacket, 100);
    if (len > 0) {
      incomingPacket[len] = 0;
    }
    Serial.println(incomingPacket);
    if (!strncmp(incomingPacket, strcmpr, 4)) { // throw out hub-specific packets
      if (loopCounter >= (1000 / DELAY)) {
        // hub:<location>:<IP>
        char *token;
        token = strtok(incomingPacket, delimiter);
        loopCounter = 0;
      }
    } else {
      udp.beginPacketMulticast(statsdIP, statsdPort, WiFi.localIP());
      udp.write(incomingPacket);
      udp.endPacket();
    }
    loopCounter++;
    delay(DELAY);
  }
}

