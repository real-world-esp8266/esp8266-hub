# ESP8266 Hub

## Statsd relay? Specialized wifi repeater? Not sure

### Purpose

I didn't want all my ESP8266's on the network with the rest of the devices on it, so I made one a "server" and the rest "clients"; all the other ESP8266's will send a UDP packet to the hub, which in turn will just send it back to the statsd server on the local network. The hub may also be in charge of displaying information, etc.

### Setup

1. `git clone`
2. Select desired branch
3. Replace necessary variables
4. Upload and provide power :)
5. May be necessary to click the "reset" button after uploading

### Testing

1. Connect to your local network
2. Connect a metric-making ESP8266 to the AP provided by the hub
3. Send a UDP packet to the hub from both the local and created network
4. Statsd should receive the packet

### Notes

There is a little delay as the packet makes the trip from the beginning to the end, but nothing too serious.

When adding a new ESP8266 to the hub, make sure that ESP8266 sends an identity packet occasionally.

### In Progress
